
provider "aws" {
  region      = "${var.region}"
  profile     = "${var.profile}"
}

resource "aws_route53_record" "one-yml-production" {
  zone_id = "ZGPZXFJHFRT0"
  name    = "1yml.ymedia.in"
  type    = "A"
  alias {
    name                   = "yml-internal-alb-1103207077.us-west-2.elb.amazonaws.com"
    zone_id                = "Z1H1FL5HABSF5"
    evaluate_target_health = false
  }
}

// SlackBot for Production
resource "aws_route53_record" "one-yml-slackbot" {
  zone_id = "ZGPZXFJHFRT0"
  name    = "1yml-sb.ymedia.in"
  type    = "A"
  alias {
    name                   = "yml-internal-alb-1103207077.us-west-2.elb.amazonaws.com"
    zone_id                = "Z1H1FL5HABSF5"
    evaluate_target_health = false
  }
}

//ALB listener rule for slackbot
resource "aws_alb_listener_rule" "one-yml-listener-rule-www-slackbot" {
  listener_arn = "${var.listener_arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.one-yml-slackbot.id}"
  }

  condition {
    host_header{
      values = ["1yml-sb.ymedia.in"]
    }
  }
}

//Alb target group for slackbot
resource "aws_lb_target_group" "one-yml-slackbot" {
  name     = "one-yml-sb-tg"
  port     = 5000
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
}

resource "aws_lb_target_group_attachment" "one-yml-slackbot" {
  target_group_arn = "${aws_lb_target_group.one-yml-slackbot.arn}"
  target_id        = "${module.one-yml-ec2.instance_ids[0]}"
  port             = "5000"
}

//ALB Listners and traget group
resource "aws_alb_listener_rule" "one-yml-listener-rule-www-production" {
  listener_arn = "${var.listener_arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.one-yml-production.id}"
  }

  condition {
    host_header{
      values = ["1yml.ymedia.in"]
    }
  }
}

resource "aws_lb_target_group" "one-yml-production" {
  name     = "one-yml-production-tg"
  port     = 4200
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
}

resource "aws_lb_target_group_attachment" "one-yml-production" {
  target_group_arn = "${aws_lb_target_group.one-yml-production.arn}"
  target_id        = "${module.one-yml-ec2.instance_ids[0]}"
  port             = "4200"
}

// EC2 instance for Production
module "one-yml-ec2" {
  source = "git::ssh://git@bitbucket.org/ymedialabs/terraform-modules.git//modules//ec2?ref=v0.42"
  subnet_id = "${var.subnet_id}"
  vpc_id = "${var.vpc_id}"
  instance_name         = "one-yml-prod"
  ami_id                = "${var.1yml_ami_id}"
  instance_type         = "t3.small"
  key_name              = "1yml-prod"
  allow_inbound_ips     = ["22, 0.0.0.0/0"]
  inbound_sgs_count     = 3
  allow_inbound_sgs     = [ "4200, ${var.alb_sg_id}", 
                            "4210, ${var.alb_sg_id}",
                            "5000, ${var.alb_sg_id}"] #ToDo: Use variable for hard codeded sg here.
                            
  volume_size           = 20
  iam_instance_profile  = "${aws_iam_instance_profile.prod-instance-profile.name}"
  additional_tags {
    company = "yml"
    project = "one-yml"
    managed-by = "terraform"
    one-yml-production = "v1"
  }
}


/*
resource "aws_lb_target_group" "one-yml-backend-production" {
  name     = "one-yml-backend-production-tg"
  port     = 4210
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
}

resource "aws_alb_listener_rule" "one-yml-listener-rule-backend-production" {
  listener_arn = "${var.listener_arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.one-yml-backend-production.id}"
  }

  condition {
    host_header{
      values = ["1yml-backend-prod.ymedia.in"]
    }
  }
}
resource "aws_lb_target_group_attachment" "one-yml-backend-production" {
  target_group_arn = "${aws_lb_target_group.one-yml-backend-production.arn}"
  target_id        = "${module.one-yml-ec2.instance_ids[0]}"
  port             = "4210"
}

*/

/*
resource "aws_codedeploy_app" "one-yml-cd-app" {
  name = "one-yml"
}


// Public Cloud - Code Deployment
resource "aws_codedeploy_deployment_group" "one-yml-cd-dg-production" {
  deployment_group_name = "production"
  app_name              = "${aws_codedeploy_app.one-yml-cd-app.name}"
  service_role_arn      = "${aws_iam_role.codedeploy_service_role.arn}"
  deployment_config_name = "CodeDeployDefault.OneAtATime"

  ec2_tag_filter {
    type  = "KEY_AND_VALUE"
    key   = "one-yml-production"
    value = "v1"
  }
}
*/
#IAM roles and policies
data "aws_iam_policy_document" "prod-instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_instance_profile" "prod-instance-profile" {
  name  = "OneYMLProdInstanceProfile"
  role = "${aws_iam_role.prod-instance-role.name}"
}

resource "aws_iam_role" "prod-instance-role" {
  name               = "OneYmlProdInstanceRole"
  path               = "/system/"
  assume_role_policy = "${data.aws_iam_policy_document.prod-instance-assume-role-policy.json}"
}

resource "aws_iam_role_policy_attachment" "codedeploy-policy-attachment" {
    role       = "${aws_iam_role.prod-instance-role.name}"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforAWSCodeDeploy"
}

// Service role for CodeDeploy service
data "aws_iam_policy_document" "codedeploy-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codedeploy.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "parameters-access-policy-web" {
  name        = "OneYMLProdParametersAccess"
  path        = "/"
  description = "One YML and Prod Parameters access environments"
  policy = "${file("policies/1yml-production-ssm-access-policy.json")}"
}

resource "aws_iam_role_policy_attachment" "parameters-access-policy-attachment-web" {
  role       = "${aws_iam_role.prod-instance-role.name}"
  policy_arn = "${aws_iam_policy.parameters-access-policy-web.arn}"
}

/*
resource "aws_iam_role" "codedeploy_service_role" {
  name = "OneYMLCodeDeployServiceRole"
  assume_role_policy = "${data.aws_iam_policy_document.codedeploy-assume-role-policy.json}"
}

resource "aws_iam_role_policy_attachment" "codedeploy_service_role" {

  role      = "${aws_iam_role.codedeploy_service_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
}
*/
