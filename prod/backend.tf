terraform {
  backend "s3" {
    bucket = "yml-terraform-state-storage"
    key    = "1yml/prod/terraform.tfstate"
    region = "us-west-2"
    profile = "yml"
  }
}
