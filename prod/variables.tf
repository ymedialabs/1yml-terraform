variable "region" {default="us-west-2"}
variable "profile" {default="yml"}
variable "vpc_id" {default="vpc-f8b9ac9a"}
variable "subnet_id" {default="subnet-4a83b63e"}
variable "listener_arn" {default="arn:aws:elasticloadbalancing:us-west-2:109460623949:listener/app/yml-internal-alb/0182db5885f56b26/6091afec11432674"}
variable "1yml_ami_id" {default="ami-0f03e6d99ac93a4ac"}
variable  "alb_sg_id" {default="sg-09831d75"}
