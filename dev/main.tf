
provider "aws" {
  region      = "${var.region}"
  profile     = "${var.profile}"
}

resource "aws_route53_record" "one-yml" {
  zone_id = "ZGPZXFJHFRT0"
  name    = "1yml-dev.ymedia.in"
  type    = "A"
  alias {
    name                   = "yml-internal-alb-1103207077.us-west-2.elb.amazonaws.com"
    zone_id                = "Z1H1FL5HABSF5"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "one-yml-backend" {
  zone_id = "ZGPZXFJHFRT0"
  name    = "1yml-backend-dev.ymedia.in"
  type    = "A"
  alias {
    name                   = "yml-internal-alb-1103207077.us-west-2.elb.amazonaws.com"
    zone_id                = "Z1H1FL5HABSF5"
    evaluate_target_health = false
  }
}

resource "aws_alb_listener_rule" "one-yml-listener-rule-www" {
  listener_arn = "${var.listener_arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.one-yml.id}"
  }

  condition {
    host_header{
      values = ["1yml-dev.ymedia.in"]
    }
  }
}

resource "aws_lb_target_group" "one-yml" {
  name     = "one-yml-dev-tg"
  port     = 7770
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
}

resource "aws_lb_target_group" "one-yml-backend" {
  name     = "one-yml-backend-dev-tg"
  port     = 27017
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
}

resource "aws_alb_listener_rule" "one-yml-listener-rule-backend" {
  listener_arn = "${var.listener_arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.one-yml-backend.id}"
  }

  condition {
    host_header{
      values = ["1yml-backend-dev.ymedia.in"]
    }
  }
}

resource "aws_lb_target_group_attachment" "one-yml" {
  target_group_arn = "${aws_lb_target_group.one-yml.arn}"
  target_id        = "${module.one-yml-ec2.instance_ids[0]}"
  port             = "7770"
}

resource "aws_lb_target_group_attachment" "one-yml-backend" {
  target_group_arn = "${aws_lb_target_group.one-yml-backend.arn}"
  target_id        = "${module.one-yml-ec2.instance_ids[0]}"
  port             = "27017"
}

module "one-yml-ec2" {
  source = "git::ssh://git@bitbucket.org/ymedialabs/terraform-modules.git//modules//ec2?ref=v0.42"
  subnet_id = "${var.subnet_id}"
  vpc_id = "${var.vpc_id}"
  instance_name         = "one-yml-dev"
  ami_id                = "${var.1yml_ami_id}"
  instance_type         = "t3.small"
  key_name              = "1yml-dev"
  allow_inbound_ips     = ["22, 0.0.0.0/0"]
  inbound_sgs_count     = 2
  allow_inbound_sgs     = [ "7770, ${var.alb_sg_id}", 
                            "27017, ${var.alb_sg_id}",
                            "5000, ${var.alb_sg_id}"] #ToDo: Use variable for hard codeded sg here.
  volume_size           = 20
  iam_instance_profile  = "${aws_iam_instance_profile.dev-instance-profile.name}"
  additional_tags {
    company = "yml"
    project = "one-yml"
    managed-by = "terraform"
    one-yml-dev = "v1"
  }
}

resource "aws_codedeploy_app" "one-yml-cd-app" {
  name = "one-yml"
}

resource "aws_codedeploy_deployment_group" "one-yml-cd-dg-dev" {
  deployment_group_name = "dev"
  app_name              = "${aws_codedeploy_app.one-yml-cd-app.name}"
  service_role_arn      = "${aws_iam_role.codedeploy_service_role.arn}"
  deployment_config_name = "CodeDeployDefault.OneAtATime"

  ec2_tag_filter {
    type  = "KEY_AND_VALUE"
    key   = "one-yml-dev"
    value = "v1"
  }
}

#IAM roles and policies
data "aws_iam_policy_document" "dev-instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_instance_profile" "dev-instance-profile" {
  name  = "OneYMLDevInstanceProfile"
  role = "${aws_iam_role.dev-instance-role.name}"
}

resource "aws_iam_role" "dev-instance-role" {
  name               = "OneYmlDevInstanceRole"
  path               = "/system/"
  assume_role_policy = "${data.aws_iam_policy_document.dev-instance-assume-role-policy.json}"
}

resource "aws_iam_role_policy_attachment" "codedeploy-policy-attachment" {
    role       = "${aws_iam_role.dev-instance-role.name}"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforAWSCodeDeploy"
}

// Service role for CodeDeploy service
data "aws_iam_policy_document" "codedeploy-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codedeploy.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codedeploy_service_role" {
  name = "OneYMLCodeDeployServiceRole"
  assume_role_policy = "${data.aws_iam_policy_document.codedeploy-assume-role-policy.json}"
}

resource "aws_iam_role_policy_attachment" "codedeploy_service_role" {

  role      = "${aws_iam_role.codedeploy_service_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
}

resource "aws_iam_policy" "parameters-access-policy-web" {
  name        = "OneYMLDevStagingParametersAccess"
  path        = "/"
  description = "One YML and staging Parameters access for dev/staging environments"
  policy = "${file("policies/parameter-store-access-policy-web.json")}"
}

resource "aws_iam_role_policy_attachment" "parameters-access-policy-attachment-web" {
  role       = "${aws_iam_role.dev-instance-role.name}"
  policy_arn = "${aws_iam_policy.parameters-access-policy-web.arn}"
}

/*
// SlackBot for Dev
resource "aws_route53_record" "one-yml-slackbot" {
  zone_id = "ZGPZXFJHFRT0"
  name    = "1yml-slackbot.ymedia.in"
  type    = "A"
  alias {
    name                   = "yml-internal-alb-1103207077.us-west-2.elb.amazonaws.com"
    zone_id                = "Z1H1FL5HABSF5"
    evaluate_target_health = false
  }
}

resource "aws_alb_listener_rule" "one-yml-listener-rule-www-slackbot" {
  listener_arn = "${var.listener_arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.one-yml-slackbot.id}"
  }

  condition {
    host_header{
      values = ["1yml-slackbot.ymedia.in"]
    }
  }
}

resource "aws_lb_target_group" "one-yml-slackbot" {
  name     = "one-yml-slackbot-tg"
  port     = 5000
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
}

resource "aws_lb_target_group_attachment" "one-yml-slackbot" {
  target_group_arn = "${aws_lb_target_group.one-yml-slackbot.arn}"
  target_id        = "${module.one-yml-ec2.instance_ids[0]}"
  port             = "5000"
}

// Public Cloud - DNS Record
resource "aws_route53_record" "one-yml-production" {
  zone_id = "ZGPZXFJHFRT0"
  name    = "1yml-production.ymedia.in"
  type    = "A"
  alias {
    name                   = "yml-internal-alb-1103207077.us-west-2.elb.amazonaws.com"
    zone_id                = "Z1H1FL5HABSF5"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "one-yml-backend-production" {
  zone_id = "ZGPZXFJHFRT0"
  name    = "1yml-backend-production.ymedia.in"
  type    = "A"
  alias {
    name                   = "yml-internal-alb-1103207077.us-west-2.elb.amazonaws.com"
    zone_id                = "Z1H1FL5HABSF5"
    evaluate_target_health = false
  }
}

// Public Cloud - ALB Listners
resource "aws_alb_listener_rule" "one-yml-listener-rule-www-production" {
  listener_arn = "${var.listener_arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.one-yml-production.id}"
  }

  condition {
    host_header{
      values = ["1yml-production.ymedia.in"]
    }
  }
}

resource "aws_lb_target_group" "one-yml-production" {
  name     = "one-yml-production-tg"
  port     = 4200
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
}

resource "aws_lb_target_group" "one-yml-backend-production" {
  name     = "one-yml-backend-production-tg"
  port     = 4210
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
}

resource "aws_alb_listener_rule" "one-yml-listener-rule-backend-production" {
  listener_arn = "${var.listener_arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.one-yml-backend-production.id}"
  }

  condition {
    host_header{
      values = ["1yml-backend-prod.ymedia.in"]
    }
  }
}

resource "aws_lb_target_group_attachment" "one-yml-production" {
  target_group_arn = "${aws_lb_target_group.one-yml-production.arn}"
  target_id        = "${module.one-yml-ec2.instance_ids[0]}"
  port             = "4200"
}

resource "aws_lb_target_group_attachment" "one-yml-backend-production" {
  target_group_arn = "${aws_lb_target_group.one-yml-backend-production.arn}"
  target_id        = "${module.one-yml-ec2.instance_ids[0]}"
  port             = "4210"
}

// Public Cloud - Code Deployment
resource "aws_codedeploy_deployment_group" "one-yml-cd-dg-production" {
  deployment_group_name = "production"
  app_name              = "${aws_codedeploy_app.one-yml-cd-app.name}"
  service_role_arn      = "${aws_iam_role.codedeploy_service_role.arn}"
  deployment_config_name = "CodeDeployDefault.OneAtATime"

  ec2_tag_filter {
    type  = "KEY_AND_VALUE"
    key   = "one-yml-production"
    value = "v1"
  }
}
*/


